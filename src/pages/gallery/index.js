import React, { useState, useEffect, useRef } from 'react'
import { FixedSizeList as List } from 'react-window'
import { fetchImagesFromBackend, likeImage, fetchLikedImages, deleteImage, uploadImage } from '../../services/imageService'
import LoadingSpinner from '../../components/loadingSpinner'
import FileUploadInput from '../../components/fileUploadInput'
import styles from './index.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Gallery = () => {
  const containerRef = useRef(null)
  const [images, setImages] = useState([])
  const [loading, setLoading] = useState(true)
  const [selectedImage, setSelectedImage] = useState(null)
  const [likedImages, setLikedImages] = useState([]) // Track the list of liked images
  const [selectedImageLoading, setSelectedImageLoading] = useState(false) // Track if the selected image is loading
  const [containerHeight, setContainerHeight] = useState(0) // Track if the selected image is loading

  const getRealHeight = () => {
    if (containerRef.current) {
      return containerRef.current.clientHeight
    }
    return 0
  }

  useEffect(() => {
    // Fetch images from the backend when the component mounts
    fetchImagesFromBackend()
      .then(imageUrls => {
        setImages(imageUrls)
      })
      .finally(() => setLoading(false))

    // Fetch the list of liked images
    fetchLikedImages()
      .then(likedImageUrls => {
        setLikedImages(likedImageUrls)
      })
      .catch(error => {
        console.error('Error fetching liked images:', error)
      })

    setContainerHeight(getRealHeight())
  }, [])

  const handleImageClick = imageUrl => {
    setSelectedImageLoading(true) // Set loading state to true before loading the selected image
    setSelectedImage(imageUrl)
  }

  const handleLikeImage = imageUrl => {
    likeImage(imageUrl).then(response => {
      if (response.liked) {
        // If the image was liked successfully, update the list of liked images
        setLikedImages(prevLikedImages => [...prevLikedImages, imageUrl])
      } else {
        // If the image was unliked, remove it from the list of liked images
        setLikedImages(prevLikedImages => prevLikedImages.filter(image => image !== imageUrl))
      }
    })
  }

  const handleDeleteImage = imageUrl => {
    deleteImage(imageUrl)
      .then(response => {
        if (response.success) {
          setImages(prevImages => prevImages.filter(image => image !== imageUrl))
          setSelectedImage(null)
        }
      })
      .catch(error => {
        console.error('Error deleting image:', error)
      })
  }

  const handleImageUpload = file => {
    if (!file) return

    const formData = new FormData()
    formData.append('image', file)

    // Send the image to the backend for upload
    uploadImage(formData)
      .then(response => {
        setImages(prevImages => [response.imageUrl, ...prevImages])
      })
      .catch(error => {
        console.error('Error uploading image:', error)
      })
  }

  useEffect(() => {
    if (selectedImage) {
      setSelectedImageLoading(true) // Set loading state to true before loading the selected image
      const img = new Image()
      img.onload = () => {
        setSelectedImageLoading(false) // Set loading state to false after loading the selected image
      }
      img.onerror = () => {
        setSelectedImageLoading(false) // Set loading state to false if there was an error loading the selected image
      }
      img.src = `http://localhost:4000/image/${selectedImage}`
    }
  }, [selectedImage])

  // Virtualized list props
  const Row = ({ index, style }) => {
    const imageUrl = images[index]
    return (
      <li style={style} key={index} className={selectedImage === imageUrl ? styles.selected : ''}>
        <FontAwesomeIcon
          icon='heart'
          className={styles.likeIcon + (likedImages?.includes(imageUrl) ? ' ' + styles.liked : '')}
          onClick={() => handleLikeImage(imageUrl)}
        />
        <span onClick={() => handleImageClick(imageUrl)}>{imageUrl.split('.JPG')[0]}</span>
        {selectedImage === imageUrl && <FontAwesomeIcon icon='trash' className={styles.deleteImage} onClick={() => handleDeleteImage(imageUrl)} />}
      </li>
    )
  }

  return (
    <div ref={containerRef} className={styles.container}>
      <ul className={styles.list}>
        {loading && <LoadingSpinner />}
        <FileUploadInput onChange={handleImageUpload} />
        <List height={containerHeight} itemCount={images.length} itemSize={20} width='100%'>
          {Row}
        </List>
      </ul>
      {selectedImage && (
        <div className={styles.imageContainer}>
          {selectedImageLoading && <LoadingSpinner />}
          <img src={`http://localhost:4000/image/${selectedImage}`} alt={selectedImage} />
        </div>
      )}
    </div>
  )
}

export default Gallery
