import React from 'react'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import '@fortawesome/fontawesome-svg-core/styles.css' // Import the Font Awesome CSS

import '../../src/app/globals.css'
import Navbar from '../app/Navbar/navbar'

// Add all solid icons to the library
library.add(fas)

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Navbar />
      <Component {...pageProps} />
    </>
  )
}

export default MyApp
