import Image from 'next/image'
import styles from './index.module.css'

export default function Home() {
  return (
    <div className={styles.main}>
      <h1>Digital twins of the real world</h1>
      <h3>Manage your assets from any location, anytime.</h3>
      <Image className={styles.logo} src='/next.svg' alt='Next.js Logo' width={180} height={37} priority />
    </div>
  )
}
