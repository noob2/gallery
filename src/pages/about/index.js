import React from 'react'
import styles from './index.module.css'

const About = () => {
  return (
    <div className={styles.main}>
      <h2>This is my first React project, and I'm excited to showcase my skills through this task</h2>
      <h3>Thank you 🙏 for considering me for this opportunity, and I'm looking forward to becoming a valuable member of the MYX team!</h3>
    </div>
  )
}

export default About
