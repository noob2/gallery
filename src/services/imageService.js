import axios from 'axios'

const fetchImagesFromBackend = () => {
  return axios
    .get('http://localhost:4000/allImages')
    .then(response => response.data.images)
    .catch(error => {
      console.error('Error fetching images:', error)
      return []
    })
}

const likeImage = imageName => {
  return axios
    .post('http://localhost:4000/likeImage/' + imageName)
    .then(response => response.data)
    .catch(error => {
      console.error('Error liking image:', error)
      return []
    })
}

const fetchLikedImages = () => {
  return axios
    .get('http://localhost:4000/likedImages')
    .then(response => response.data.likedImages)
    .catch(error => {
      console.error('Error liking image:', error)
      return []
    })
}

const deleteImage = imageUrl => {
  return axios
    .delete(`http://localhost:4000/image/${imageUrl}`)
    .then(response => response.data)
    .catch(error => {
      console.error('Error deleting image:', error)
      throw error
    })
}

const uploadImage = formData => {
  return axios
    .post('http://localhost:4000/uploadImage', formData)
    .then(response => response.data)
    .catch(error => {
      console.error('Error uploading image:', error)
      throw error
    })
}

export { fetchImagesFromBackend, likeImage, fetchLikedImages, deleteImage, uploadImage }
