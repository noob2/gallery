import React from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import styles from './navbar.module.css'

const Navbar = () => {
  const router = useRouter()

  return (
    <nav className={styles.menu}>
      <ul>
        <li className={router.pathname === '/' ? styles.active : ''}>
          <Link href='/'>Home</Link>
        </li>
        <li className={router.pathname === '/gallery' ? styles.active : ''}>
          <Link href='/gallery'>Gallery</Link>
        </li>
        <li className={router.pathname === '/about' ? styles.active : ''}>
          <Link href='/about'>About</Link>
        </li>
      </ul>
    </nav>
  )
}

export default Navbar
