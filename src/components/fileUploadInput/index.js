import React from 'react'
import './fileUploadInput.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default function FileUploadInput({ onChange }) {
  const handleFileChange = event => {
    console.log(event)
    const file = event.target?.files[0]
    if (!file) return

    onChange(file) // Call the onChange function with the selected file
  }

  return (
    <label className='label'>
      <input type='file' required accept='.jpg, .jpeg, .png, .gif' onChange={handleFileChange} />
      <FontAwesomeIcon icon='plus' />
    </label>
  )
}
